'use strict';

describe('myApp.polygon_example module', function() {

  beforeEach(module('myApp.polygon_example'));

  describe('polygon_example controller', function(){
   
    it('should ....', inject(function($controller) {
      //spec body
      var polygon_exampleCtrl = $controller('Polygon_exampleCtrl');
 
      expect(polygon_exampleCtrl).toBeDefined();

    }));

  }); 
});