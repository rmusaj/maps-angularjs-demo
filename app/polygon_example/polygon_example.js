'use strict';
angular.module('myApp.polygon_example', ['ngRoute'] )
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/polygon_example', {
            templateUrl: 'polygon_example/polygon_example.html'
        });
    }]) 

    .controller('Polygon_exampleCtrl',function () {

    var vm = this;
    
    vm.onMapOverlayCompleted = function (e) {
        if(vm.polygon)
            vm.polygon.setMap(null); 
         vm.polygon = e.overlay;          
        };

      vm.clearAll = function() {
          vm.polygon.setMap(null);      
           vm.map.mapDrawingManager.drawingControl =  true;
      } 

     vm.$onDestroy = function () {
         if(vm.polygon)
     vm.polygon.setMap(null); 

  };  

    });