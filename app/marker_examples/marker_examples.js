'use strict';
angular.module('myApp.marker_examples', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/marker_examples', {
            templateUrl: 'marker_examples/marker_examples.html',
            controller: 'Marker_examplesCtrl',
            controllerAs: "vm"
        });
    }])

    .controller('Marker_examplesCtrl',  function (NgMap) {

      var vm = this;
   NgMap.getMap().then(map => 
   { vm.map = map;
       console.log(vm.map.markers);
       console.log(vm.map.shapes);
   
   }
    );
      vm.addMarker = function(event) {
        var ll = event.latLng;
        vm.position = {lat:ll.lat(), lng: ll.lng()};
      }

      vm.deleteMarkers = function() {

      vm.position = null;

      };
    
    });