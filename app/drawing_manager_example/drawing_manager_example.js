'use strict';

angular.module('myApp.drawing_manager_example', ['ngRoute'])
 
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/drawing_manager_example', {
            templateUrl: 'drawing_manager_example/drawing_manager_example.html',
            controller: 'Drawing_manager_exampleCtrl',
            controllerAs: "vm"
        });
    }])

    .controller('Drawing_manager_exampleCtrl', function (NgMap) {

        var vm = this;  
           NgMap.getMap().then(map => 
   { vm.map = map;
       console.log(vm.map.markers);
       console.log(vm.map.shapes);
   
   }
    );      
        vm.onMapOverlayCompleted = function (e) {
            
            vm.content = '';
            vm.content += 'Is a ' + e.type;

            if (e.type == "marker") {
                vm.content += " Position = { lat " + e.overlay.position.lat() + " , lng: " + e.overlay.position.lng() + "}";
            }

            if (e.type == "polyline") {
                vm.content += " Path = [" + e.overlay.getPath().b + "]";
            }

            if (e.type == "polygon") {
                // Since this polygon has only one path, we can call getPath() to return the
                // MVCArray of LatLngs.
                var vertices = e.overlay.getPath();

                vm.content += '[ ';

                // Iterate over the vertices.
                for (var i = 0; i < vertices.getLength(); i++) {
                    var xy = vertices.getAt(i);
                    vm.content += ' { ' + xy.lat() + ',' +
                        xy.lng() + ' }';
                }
                vm.content += ' ]';
            }

            if (e.type == "rectangle") {
                vm.content += " Bounds = [" + e.overlay.getBounds() + "]";
            }

            if (e.type == "circle") {
                vm.content += " Center = " + e.overlay.getCenter() + " ";
                vm.content += " Radius = " + e.overlay.getRadius() + " ";
            }

        };
    });