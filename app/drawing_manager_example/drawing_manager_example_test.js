'use strict';

describe('myApp.drawing_manager_example module', function() {

  beforeEach(module('myApp.drawing_manager_example'));

  describe('drawing_manager_example controller', function(){
 
    it('should ....', inject(function($controller) {
      //spec body
      var drawing_manager_exampleCtrl = $controller('drawing_manager_exampleCtrl');
      expect(drawing_manager_exampleCtrl).toBeDefined();
    }));

  }); 
});