# `angularjs Map demo` � a simple demo how to use ngmap library

## Installation

1. For bower user : $ bower install ngmap
2. For npm users: $ npm install ngmap


## Getting Started

1.	Include ng-map.min.js: <script src="/bower_components/ngmap/build/scripts/ng-map.min.js"></script>
2.	Include Google maps: <script src="http://maps.google.com/maps/api/js"></script>
3.	Name your angular app ngMap, or add it as a dependency var myApp = angular.module('myApp', ['ngMap']);+

### Directives to be study

1.	Bicycling-layer
2.	Custom-control
3.	Dircetions
4.	Drawing manager
5.	Dynamic maps egine layer
6.	Fusion tables layer
7.	Heatmap layer
8.	Info windows
9.	Klm layer
10.	Map data
11.	Map lazy load
12.	Map type
13.	Map
14.	Maps engine layer
15.	Marker
16.	Overlay map type
17.	Places auto complete
18.	Shape
19.	Street view panorama
20.	Traffic layer
21.	Transit layer


### Resources 

1. https://github.com/allenhwkim/angularjs-google-maps
2. http://allenhwkim.tumblr.com/post/70986888283/google-map-as-the-simplest-way
3. https://rawgit.com/allenhwkim/angularjs-google-maps/master/testapp/marker-clusterer.html
4. https://rawgit.com/allenhwkim/angularjs-google-maps/master/testapp/map_app.html
5. https://rawgit.com/allenhwkim/angularjs-google-maps/master/testapp/custom-marker-2.html
6. https://ngmap.github.io/#/!layer-data-simple.html
��..

